#include <stdio.h>

int main()
{
    int s,l,n,i,t;
    printf("Enter size of array \n");
    scanf("%d",&n);
    int arr[n];
    printf("Enter elements of the array\n");
    for(i=0;i<n;i++)
    scanf("%d",&arr[i]);
    s=0;
    l=0;
	
    printf("Array before swapping :\n ");
    for(i=0;i<n;i++)
    printf("%d ",arr[i]);
    for(i=1;i<n;i++)
    {
        if(arr[i]<arr[s])
        s=i;
        if(arr[i]>arr[l])
        l=i;
    }
    t=arr[s];
    arr[s]=arr[l];
    arr[l]=t;
    printf("\nArray after swapping :\n ");
    for(i=0;i<n;i++)
    printf("%d ",arr[i]);
    return 0;
}